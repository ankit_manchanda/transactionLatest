package com.ing.exchange.plateform.exception;

import java.util.List;

public class ValidationErrors extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final List<Error> errorList;

	public ValidationErrors(List<Error> errorList) {
		super(errorList.get(0).getMessage());
		this.errorList = errorList;
	}

	public List<Error> getErrorList() {
		return errorList;
	}
}
