package com.ing.exchange.plateform.service;

import java.util.List;

import com.ing.exchange.plateform.entity.TransactionEntity;
import com.ing.exchange.plateform.model.TransactionRequest;

public interface ExchangeService {

	public List<TransactionEntity> retrieveTransaction(String userId, String type);

	public Integer submitTransaction(TransactionRequest request);

	public Integer receieveTransaction(TransactionRequest request);

}
