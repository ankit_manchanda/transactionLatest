package com.ing.exchange.plateform.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class TransactionRequest {

	@JsonProperty("sendingUserId")
	private String sendingId;
	
	@JsonProperty("receivingUserId")
	private String receivingId;
	
	
	@JsonProperty("amount")
	private Integer totalAmount;
	
	
	@JsonProperty("description")
	private String desc;
	
	
	@JsonProperty("timePlaced")
	private String time;


	/**
	 * @return the sendingId
	 */
	public String getSendingId() {
		return sendingId;
	}


	/**
	 * @param sendingId the sendingId to set
	 */
	public void setSendingId(String sendingId) {
		this.sendingId = sendingId;
	}


	/**
	 * @return the receivingId
	 */
	public String getReceivingId() {
		return receivingId;
	}


	/**
	 * @param receivingId the receivingId to set
	 */
	public void setReceivingId(String receivingId) {
		this.receivingId = receivingId;
	}


	/**
	 * @return the totalAmount
	 */
	public Integer getTotalAmount() {
		return totalAmount;
	}


	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}


	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}


	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}


	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}


	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	
}
