package com.ing.exchange.plateform.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class TransactionResponse {

	@JsonProperty("code")
	private Integer statusCode;
	
	@JsonProperty("message")
	private String statusMessage;
	
	@JsonProperty("records")
	private List<TransactionHistory> history;
	
	

	/**
	 * @return the history
	 */
	public List<TransactionHistory> getHistory() {
		return history;
	}

	/**
	 * @param history the history to set
	 */
	public void setHistory(List<TransactionHistory> history) {
		this.history = history;
	}

	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
}
