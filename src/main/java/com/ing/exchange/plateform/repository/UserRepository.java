package com.ing.exchange.plateform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ing.exchange.plateform.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, String> {

	@Query(value="FROM UserEntity where user_id=:userid")
	UserEntity findByUserId(@Param("userid") String userId);
}
