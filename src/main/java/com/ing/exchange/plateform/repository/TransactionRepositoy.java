package com.ing.exchange.plateform.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ing.exchange.plateform.entity.TransactionEntity;

public interface TransactionRepositoy extends JpaRepository<TransactionEntity, Integer> {

	@Query(value="FROM TransactionEntity where user_id=:userid")
	List<TransactionEntity> findBasedOnTransactionTypeDebited(@Param("userid") String userId);
	
	
	@Query(value="FROM TransactionEntity where other_user_id=:userid")
	List<TransactionEntity> findBasedOnTransactionTypeCredited(@Param("userid") String userId);
	
	@Query(value="FROM TransactionEntity where user_id=:userid or other_user_id=:userid")
	List<TransactionEntity> findForAllTransactions(@Param("userid") String userId);
}
