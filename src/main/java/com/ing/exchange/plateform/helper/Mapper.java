package com.ing.exchange.plateform.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ing.exchange.plateform.entity.TransactionEntity;
import com.ing.exchange.plateform.model.TransactionHistory;
import com.ing.exchange.plateform.model.TransactionRequest;
import com.ing.exchange.plateform.model.TransactionResponse;


@Component
public class Mapper {

	/**
	 * This method will convert transaction request to transaction entity
	 * @param request
	 * @return
	 */
	public TransactionEntity convertRequestToTransactionEntity(TransactionRequest request) {
		TransactionEntity entity = new TransactionEntity();
		entity.setAmountSent(request.getTotalAmount());
		entity.setDescription(request.getDesc());
		entity.setReceivingUser(request.getReceivingId());
		entity.setSendingUser(request.getSendingId());
		entity.setTimePlaced(request.getTime());
		return entity;
	}
	
	/**
	 * This method will convert Transaction entity list into history list.
	 * @param entityList
	 * @param userId 
	 * @return
	 */
	public List<TransactionHistory> convertTransactionEntityToHistoryList(List<TransactionEntity> entityList, String userId) {
		List<TransactionHistory> historyList = new ArrayList<>();
		if(!entityList.isEmpty()) {
			for (TransactionEntity transactionEntity : entityList) {
				TransactionHistory history = new TransactionHistory();
				history.setDesc(transactionEntity.getDescription());
				history.setTime(transactionEntity.getTimePlaced());
				history.setTotalAmount(transactionEntity.getAmountSent());
				if(userId.equalsIgnoreCase(transactionEntity.getReceivingUser())) {
					history.setTransactionType(TransactionType.C.toString()); 
					history.setUser(transactionEntity.getReceivingUser()); 
				} else {
					history.setTransactionType(TransactionType.D.toString());
					history.setUser(transactionEntity.getSendingUser());
				}
				
				historyList.add(history);
			}			
		}
		return historyList;
		
	}
	
	/**
	 * This method will convert transaction history to transaction response.
	 * @param transactionHistory
	 * @return
	 */
	public TransactionResponse createTransactionResponse(List<TransactionHistory> transactionHistory) {
		TransactionResponse response = new TransactionResponse();
		if(!transactionHistory.isEmpty()) {
			response.setStatusCode(200);
			response.setStatusMessage("Transaction Successful");
			response.setHistory(transactionHistory);
		}  else {
			response.setStatusCode(400);
			response.setStatusMessage("Transaction Failed");
		}
		return response;
	}
	
	/**
	 * This method will convert transaction history to transaction response.
	 * @param transactionHistory
	 * @return
	 */
	public TransactionResponse createTransactionResponse(Integer val) {
		TransactionResponse response = new TransactionResponse();
		if(val == 1) {
			response.setStatusCode(200);
			response.setStatusMessage("Transaction Successful");
		}  else {
			response.setStatusCode(400);
			response.setStatusMessage("Transaction Failed");
		}
		return response;
	}
}
