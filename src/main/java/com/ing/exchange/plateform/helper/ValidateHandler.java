package com.ing.exchange.plateform.helper;

import javax.inject.Inject;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.MessageSource;

public class ValidateHandler {

	@Inject
	private MessageSource msgSource;
	
	//Integer code = Integer.parseInt(msgSource.getMessage(errorCode+".CODE", null, null));

	
	/** 
	 * @return instance of Validator
	 * 
	 * */
	private static Validator getValidator(){
		ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory();
		
		return validatorFactory.getValidator();
	}
}
